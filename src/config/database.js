const { sequelize } = require('./vars');

export default {
  development: {
    sequelize,
  },
};
